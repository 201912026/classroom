depoloyed on:

https://aqueous-beyond-56381.herokuapp.com/home

# MohammadZen Mansuri
# DAIICT - 201912026

**Database** MySql h2Database

Run on **Spring Boot**
main class: `com.example.toddle.ToddleApplication`

Postman Json file Attached in `/postman/Toddle.postman_collection.json`

API calls in sequence:

1. dummy data `localhost:8080/home` (To create dummy users)
   => 3 users created (zain:password, one:one (STUDENT), two:two (STUDENT))

2. login `localhost:8080/user` with body form data
    userName: zain  
    password: password (RESPONSE JwtToken)
   
3. create Assignment `localhost:8080/service/assignment/create` with body form data
    name : TASK NAME
    desc : desc
    start : 27-07-2021
    end : 5-08-2021
    students : 2,3
   
4. delete Assignment `localhost:8080/service/assignment/delete/1`
    Param Value : Assignment Id
   
-- Create New Assignment -- 
   
5. Feed for Tutor `localhost:8080/service/feed`

6. Login as Student `localhost:8080/user` with body parse from data
    userName : one
    password: one
        -- will get JwtToken
   
7. Submit Assignment `localhost:8080/service/assignment/submit` with body parse from data
   assURL : URL VALUE
   remark : REMARK
   assId : 2 (Assignment ID)
    -- (only one submission per assignment allowed)
   
8. Feed `localhost:8080/service/feed`
    
9. Get Assignment Detail `localhost:8080/service/assignment/2`

    -- Login Again as Tutor using (zain, password) and repeat step 9 to get student information. --

**If there is no response then change Update From Header Key: Authorization Value: TOKEN**



   
    
    

   



package com.example.toddle.model;

public class UserModel {

    public UserModel(String usname, String token, String type, Long id) {
        this.username = usname;
        this.token = token;
        this.type = type;
        this.id = id;
    }

    private String username;
    private String token;
    private String type;
    private Long id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

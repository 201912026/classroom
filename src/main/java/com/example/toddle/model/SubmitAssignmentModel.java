package com.example.toddle.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubmitAssignmentModel {
    private long id;
    private String name;
    private Date endOn;
    private Date startOn;
    private String status;

    public List<SubmitUserModel> getList() {
        return list;
    }

    public void setList(List<SubmitUserModel> list) {
        this.list = list;
    }

    List<SubmitUserModel> list = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getEndOn() {
        return endOn;
    }

    public void setEndOn(Date endOn) {
        this.endOn = endOn;
    }

    public Date getStartOn() {
        return startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

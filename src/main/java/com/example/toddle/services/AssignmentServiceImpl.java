package com.example.toddle.services;

import com.example.toddle.model.Assignment;
import com.example.toddle.model.AssignmentStudent;
import com.example.toddle.model.AssignmentSubmission;
import com.example.toddle.repository.AssignmentRepository;
import com.example.toddle.repository.AssignmentStudentRepository;
import com.example.toddle.repository.SubmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService{

    @Autowired
    private SubmissionRepository submissionRepository;

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Autowired
    private AssignmentStudentRepository studentRepository;


    @Override
    public void save(Assignment assignment) {
        assignmentRepository.save(assignment);
    }

    @Override
    public void save(AssignmentStudent student) {
        studentRepository.save(student);
    }

    @Override
    public void save(AssignmentSubmission submission) {
        submissionRepository.save(submission);
    }

    @Override
    public void delete(Assignment assignment) {

        // delete AssignmentStudent
        for(AssignmentStudent student : getAllByAssignmentId(assignment.getId())) {
            delete(student);
        }

//        delete AssignmentSubmission
        for(AssignmentSubmission submission : getAllSubmissionsByAssignmentId(assignment.getId())) {
            delete(submission);
        }

        assignmentRepository.delete(assignment);
    }

    @Override
    public void delete(AssignmentStudent student) {
        studentRepository.delete(student);
    }

    @Override
    public void delete(AssignmentSubmission submission) {
        submissionRepository.delete(submission);
    }

    @Override
    public List<Assignment> getAllAssignment(Long id) {
        return assignmentRepository.getAllByTutorId(id);
    }

    @Override
    public List<AssignmentSubmission> getAllTutorAssignments(Long id) {
        return submissionRepository.getAllByAssignmentId(id);
    }

    @Override
    public AssignmentSubmission getStudentAssignment(Long id, Long studId) {
        return submissionRepository.findByAssignmentId(id, studId);
    }

    @Override
    public List<AssignmentSubmission> getAllSubmissionsByAssignmentId(Long id) {
        return submissionRepository.getAllByAssignmentId(id);
    }

    @Override
    public Assignment findByName(String name) {
        return assignmentRepository.findByNameAllIgnoreCase(name).get(0);
    }

    @Override
    public Assignment findById(Long id) {
        return assignmentRepository.findById(id).orElse(new Assignment());
    }

    @Override
    public List<AssignmentStudent> getAllByAssignmentId(Long id) {
        return studentRepository.getAllAssignmentsByAssId(id);
    }



    @Override
    public AssignmentStudent findAssignmentStudentByAssignmentIdAndUserId(Long assid, Long studid) {
        return studentRepository.findAssignmentStudentByAssignmentIdAndUserId(assid, studid);
    }

    @Override
    public List<AssignmentStudent> getAllAssignmentByStudentId(Long id) {
        return studentRepository.findAllByStudentId(id);
    }
}

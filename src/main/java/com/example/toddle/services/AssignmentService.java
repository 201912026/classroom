package com.example.toddle.services;

import com.example.toddle.model.Assignment;
import com.example.toddle.model.AssignmentStudent;
import com.example.toddle.model.AssignmentSubmission;

import java.util.List;

public interface AssignmentService {

    void save(Assignment assignment);

    void save(AssignmentStudent student);

    void save(AssignmentSubmission submission);

    void delete(Assignment assignment);

    void delete(AssignmentStudent student);

    void delete(AssignmentSubmission submission);

    List<Assignment> getAllAssignment(Long id);

    List<AssignmentSubmission> getAllTutorAssignments(Long id);

    AssignmentSubmission getStudentAssignment(Long id, Long studId);

    Assignment findByName(String name);

    Assignment findById(Long id);

    List<AssignmentStudent> getAllByAssignmentId(Long id);

    List<AssignmentSubmission> getAllSubmissionsByAssignmentId(Long id);

    AssignmentStudent findAssignmentStudentByAssignmentIdAndUserId(Long assid, Long studid);

    List<AssignmentStudent> getAllAssignmentByStudentId(Long id);

}

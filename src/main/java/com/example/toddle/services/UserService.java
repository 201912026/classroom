package com.example.toddle.services;

import com.example.toddle.model.User;

public interface UserService {

    void save(User user);

    User findUserByName(String name);

    boolean verifyUser(String name, String password);

    User findByUserId(Long id);

}

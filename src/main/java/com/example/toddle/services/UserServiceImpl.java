package com.example.toddle.services;

import com.example.toddle.model.User;
import com.example.toddle.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public void save(User user) {
        repository.save(user);
    }

    @Override
    public User findUserByName(String name) {
        return repository.findUserByUsername(name);
    }

    @Override
    public boolean verifyUser(String name, String password) {
        User user = repository.findUserByUsernameAndPassword(name,password);
        if(Objects.nonNull(user) && user.getId()!=null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public User findByUserId(Long id) {
        return repository.findById(id).orElse(new User());
    }
}

package com.example.toddle.enums;

public enum ApplicationStatus {
    SCHEDULED, ONGOING
}

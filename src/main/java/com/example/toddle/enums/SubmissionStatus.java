package com.example.toddle.enums;

public enum SubmissionStatus {
    SUBMITTED, PENDING, OVERDUE, CANCELLED
}

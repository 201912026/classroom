package com.example.toddle.controller;

import com.example.toddle.model.*;
import com.example.toddle.services.AssignmentService;
import com.example.toddle.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
public class FeedController {
    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private UserService userService;

    private static final Logger log = LoggerFactory.getLogger(FeedController.class);


    @RequestMapping("service/feed")
    public ResponseEntity<Object> getAssignment(HttpServletRequest request, HttpSession session) throws JSONException, JsonProcessingException {
        User sessionUser = (User) request.getSession().getAttribute("user");
        if (Objects.nonNull(sessionUser)) {
            log.info("user found");
            if (sessionUser.getType().equalsIgnoreCase("STUDENT")) {
                log.info("Student");
//                List<JSONObject> list = new ArrayList<>();
                List<AssignmentStudent> assignments = assignmentService.getAllAssignmentByStudentId(sessionUser.getId());

                if (Objects.isNull(assignments)) {
                    return new ResponseEntity<>("Assignment ID is not valid or no assigment submitted", HttpStatus.NOT_FOUND);
                }
                List<FeedModel> list = new ArrayList<>();

                for (AssignmentStudent assignment : assignments) {
                    FeedModel model = new FeedModel();
                    model.setName(assignment.getAssignment().getName());
                    model.setDateEnd(assignment.getAssignment().getDateEnd());
                    model.setDateStart(assignment.getAssignment().getDateStart());
                    model.setAssignmentStatus(assignment.getAssignment().getStatus().toString());
                    model.setStatus(assignment.getStatus().toString());
                    model.setAssignmentId(assignment.getAssignment().getId());
                    model.setId(assignment.getId());
                    list.add(model);
                }

                return new ResponseEntity<>(list, HttpStatus.OK);
            } else {
                log.info("tutor");

                List<Assignment> assignments = assignmentService.getAllAssignment(sessionUser.getId());

                if (assignments.size() == 0) {
                    return new ResponseEntity<>("Assignment ID is not valid or no Assignment is created", HttpStatus.NOT_FOUND);
                }

                List<AssignmentModel> list = new ArrayList<>();
                for (Assignment assignment : assignments) {
                    AssignmentModel model = new AssignmentModel();
                    model.setId(assignment.getId());
                    model.setDateEnd(assignment.getDateEnd());
                    model.setDateStart(assignment.getDateStart());
                    model.setDesc(assignment.getDesc());
                    model.setName(assignment.getName());
                    model.setStatus(assignment.getStatus().toString());

                    list.add(model);
                }

                return new ResponseEntity<>(list, HttpStatus.OK);
            }
        } else {
            log.info("Something went wrong or session expired");
//            sessionUser = (User) session.getAttribute("user");

            return new ResponseEntity<>("Something went wrong", HttpStatus.BAD_GATEWAY);
        }

    }



}

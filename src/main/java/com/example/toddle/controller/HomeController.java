package com.example.toddle.controller;

import com.example.toddle.model.User;
import com.example.toddle.model.UserModel;
import com.example.toddle.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class HomeController {

    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private UserService userService;

    @Value("${config.jwt.key}")
    private final String key="mySecretKeyZain";

    @PostMapping("user")
    public ResponseEntity<Object> getUser(
            HttpServletRequest request, HttpSession session) {

        String name = request.getParameter("userName");
        String password = request.getParameter("password");

        log.info(name);

        User sessionUser = new User();
        sessionUser = userService.findUserByName(name);

        if(Objects.nonNull(sessionUser)) {
            if(userService.verifyUser(name, password)) {
                log.info("True");
                String token = getJWTToken(name);
                User user = new User();
                user.setUsername(name);
                user.setPassword(token);

                session.setAttribute("user", sessionUser);
                request.getSession().setAttribute("user", sessionUser);
                UserModel userModel = new UserModel(sessionUser.getUsername(), token, sessionUser.getType(), sessionUser.getId());
                return new ResponseEntity<>(userModel, HttpStatus.OK);
            } else {
                log.info("False");
                return new ResponseEntity<>("Something Went Wrong, Maybe password is wrong", HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<>("User Not Found", HttpStatus.NOT_FOUND);
        }


    }

    private String getJWTToken(String username) {

        String secretKey = key;
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("zain")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;

    }


}

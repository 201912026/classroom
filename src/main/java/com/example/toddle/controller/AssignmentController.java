package com.example.toddle.controller;

import com.example.toddle.enums.ApplicationStatus;
import com.example.toddle.enums.SubmissionStatus;
import com.example.toddle.model.*;
import com.example.toddle.services.AssignmentService;
import com.example.toddle.services.UserService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AssignmentController {

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private UserService userService;

    private static final Logger log = LoggerFactory.getLogger(AssignmentController.class);


    //    get Assignment

    /**
     *
     * @param id
     * @param request
     * @param session
     * @return
     * @throws JSONException
     */
    @RequestMapping("service/assignment/{id}")
    public ResponseEntity<Object> getAssignment(@PathVariable(value = "id") long id, HttpServletRequest request, HttpSession session) throws JSONException, JsonProcessingException {
        User sessionUser = (User) request.getSession().getAttribute("user");
        if (Objects.nonNull(sessionUser)) {
            log.info("user found");
            if (sessionUser.getType().equalsIgnoreCase("STUDENT")) {
                log.info("Student");
//                List<JSONObject> list = new ArrayList<>();
                AssignmentSubmission submission = assignmentService.getStudentAssignment(id, sessionUser.getId());

                if (Objects.isNull(submission)) {
                    return new ResponseEntity<>("Assignment ID is not valid or no assigment submitted", HttpStatus.NOT_FOUND);
                }

                SubmitUserModel userModel = new SubmitUserModel();
                userModel.setId(submission.getId());
                userModel.setDateCreate(submission.getDateCreated());
                userModel.setPath(submission.getPath());
                userModel.setStatus(submission.getStatus().toString());
                userModel.setRemark(submission.getRemark());
                List<SubmitUserModel> list = new ArrayList<>();
                list.add(userModel);

                SubmitAssignmentModel model = new SubmitAssignmentModel();
                model.setName(submission.getAssignment().getName());
                model.setId(submission.getAssignment().getId());
                model.setStartOn(submission.getAssignment().getDateStart());
                model.setEndOn(submission.getAssignment().getDateEnd());
                model.setStatus(submission.getAssignment().getStatus().toString());
                model.setList(list);

                return new ResponseEntity<>(model, HttpStatus.OK);
            } else {
                log.info("tutor");

                List<AssignmentSubmission> submissions = assignmentService.getAllTutorAssignments(id);

                if (submissions.size() == 0) {
                    return new ResponseEntity<>("Assignment ID is not valid or no assigment submitted", HttpStatus.NOT_FOUND);
                }

                List<SubmitUserModel> list = new ArrayList<>();

                for (AssignmentSubmission submission : submissions) {

                    SubmitUserModel userModel = new SubmitUserModel();
                    userModel.setId(submission.getId());
                    userModel.setDateCreate(submission.getDateCreated());
                    userModel.setPath(submission.getPath());
                    userModel.setStatus(submission.getStatus().toString());
                    userModel.setRemark(submission.getRemark());
                    list.add(userModel);
                }

                SubmitAssignmentModel model = new SubmitAssignmentModel();
                model.setName(submissions.get(0).getAssignment().getName());
                model.setId(submissions.get(0).getAssignment().getId());
                model.setStartOn(submissions.get(0).getAssignment().getDateStart());
                model.setEndOn(submissions.get(0).getAssignment().getDateEnd());
                model.setStatus(submissions.get(0).getAssignment().getStatus().toString());
                model.setList(list);

                return new ResponseEntity<>(model, HttpStatus.OK);
            }
        } else {
            log.info("Something went wrong or session expired");
//            sessionUser = (User) session.getAttribute("user");

            return new ResponseEntity<>("Something went wrong", HttpStatus.BAD_GATEWAY);
        }

    }


    //    Create Assignment

    /**
     *
     * @param request
     * @param response
     * @param session
     * @return
     */
    @PostMapping("service/assignment/create")
    public ResponseEntity<Object> createAssignment(HttpServletRequest request, HttpServletResponse response, HttpSession session) {


        String name = request.getParameter("name");
        String desc = request.getParameter("desc");
        String end = request.getParameter("end");
        String start = request.getParameter("start");
        String students = request.getParameter("students");

        User sessionUser = (User) request.getSession().getAttribute("user");
        if (Objects.nonNull(sessionUser)) {
            log.info("user found");
            if (sessionUser.getType().equalsIgnoreCase("STUDENT")) {
                log.info("Student");
//                Not allowed to create
                return new ResponseEntity<>("Permission Denied", HttpStatus.FORBIDDEN);
            } else {
                log.info("tutor");

                Assignment assignment = new Assignment();

                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                try {
                    assignment.setTutorId(sessionUser);
                    assignment.setName(name);
                    assignment.setDesc(desc);
                    assignment.setDateCreate(new Date());
                    Date current = new Date();
                    Date date = format.parse(start);
                    assignment.setDateStart(date);

                    if (date.before(current))
                        assignment.setStatus(ApplicationStatus.ONGOING);

                    if (date.equals(current))
                        assignment.setStatus(ApplicationStatus.ONGOING);

                    date = format.parse(end);
                    assignment.setDateEnd(date);
                    String[] studentsId = students.split(",");
                    Set<User> users = new HashSet<>();
                    for (String s : studentsId) {
                        log.info("ID: {}",s);
                        Long l = Long.parseLong(s);
                        User user = userService.findByUserId(l);
                        if (Objects.nonNull(user)) {
                            log.info("uid 1: {}", user.getId());
                            users.add(user);
                        }
                    }
                    assignment.setStudents(users);
                    assignmentService.save(assignment);

                    log.info("saved!");

                    assignment = assignmentService.findByName(name);

                    for (String s : studentsId) {
                        log.info(s);
                        Long l = Long.parseLong(s);
                        User user = userService.findByUserId(l);
                        if (Objects.nonNull(user)) {
                            log.info("uid: {}", user.getId());
                            users.add(user);

                            AssignmentStudent student = new AssignmentStudent();
                            student.setAssignment(assignment);
                            student.setUser(user);
                            student.setDateCreate(new Date());

                            assignmentService.save(student);
                        }
                    }

                    return new ResponseEntity<>(assignment, HttpStatus.OK);

                } catch (ParseException e) {
                    e.printStackTrace();
                    return new ResponseEntity<>("Failed to create", HttpStatus.BAD_REQUEST);
                }

            }
        } else {
            log.info("Something went wrong or session expired");
            return new ResponseEntity<>("Something went wrong or session expired", HttpStatus.BAD_REQUEST);
        }

    }

//    delete Assignment TUTOR

    /**
     *
     * @param id
     * @return
     * @throws JSONException
     */
    @RequestMapping("service/assignment/delete/{id}")
    public ResponseEntity<Object> deleteAssignment(@PathVariable(value = "id") long id, HttpServletRequest request, HttpSession session) throws JSONException {

        User sessionUser = (User) request.getSession().getAttribute("user");
        if (Objects.nonNull(sessionUser)) {
            log.info("user found");
            if (sessionUser.getType().equalsIgnoreCase("STUDENT")) {
                log.info("Student");
//                Not allowed to create
                return new ResponseEntity<>("Permission Denied", HttpStatus.FORBIDDEN);
            } else {
                log.info("tutor");

                Assignment assignment = assignmentService.findById(id);
                if (Objects.nonNull(assignment) && Objects.nonNull(assignment.getId())) {
                    log.info(assignment.getId().toString());
                    assignmentService.delete(assignment);
                    return new ResponseEntity<>("Assignment Deleted", HttpStatus.OK);

                } else {
                    return new ResponseEntity<>("No Assignment found", HttpStatus.NOT_FOUND);
                }

            }
        } else {
            log.info("Something went wrong or session expired");
            return new ResponseEntity<>("Something went wrong or session expired", HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * @param request
     * @param response
     * @param session
     * @return
     */
//    Submit Assignment STUDENT
    @PostMapping("service/assignment/submit")
    public ResponseEntity<Object> createAssignmentSubmission(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

        String assURL = request.getParameter("assURL");
        String remark = request.getParameter("remark");
        Long id = Long.valueOf(request.getParameter("assId"));

        User sessionUser = (User) request.getSession().getAttribute("user");
        if (Objects.nonNull(sessionUser)) {
            log.info("user found");
            if (sessionUser.getType().equalsIgnoreCase("STUDENT")) {
                log.info("Student");
//                Not allowed to create
                Assignment assignment = assignmentService.findById(id);
                if (Objects.nonNull(assignment) && Objects.nonNull(assignment.getId())) {
                    log.info(assignment.getId().toString());

                    AssignmentSubmission submission = new AssignmentSubmission();
                    AssignmentSubmission submissionPast = new AssignmentSubmission();

                    submissionPast = assignmentService.getStudentAssignment(id, sessionUser.getId());
                    if(Objects.nonNull(submissionPast)) {
                        return new ResponseEntity<>("Already Submitted", HttpStatus.ALREADY_REPORTED);
                    }

                    submission.setAssignment(assignment);
                    submission.setPath(assURL);
                    submission.setRemark(remark);
                    submission.setUser(sessionUser);
                    submission.setStatus(SubmissionStatus.SUBMITTED);
                    assignmentService.save(submission);

                    submission = assignmentService.getStudentAssignment(id, sessionUser.getId());

                    log.info(submission.getId().toString());

                    AssignmentStudent student = assignmentService.findAssignmentStudentByAssignmentIdAndUserId(assignment.getId(), sessionUser.getId());
                    student.setStatus(SubmissionStatus.SUBMITTED);
                    student.setDateModify(new Date());
                    assignmentService.save(student);

                    return new ResponseEntity<>("Assignment Submitted", HttpStatus.OK);

                } else {
                    return new ResponseEntity<>("No Assignment found", HttpStatus.NOT_FOUND);
                }
            } else {
                log.info("tutor");

                return new ResponseEntity<>("Permission Denied only student can submit", HttpStatus.FORBIDDEN);

            }
        } else {
            log.info("Something went wrong or session expired");
            return new ResponseEntity<>("Something went wrong or session expired", HttpStatus.BAD_REQUEST);
        }


    }

}

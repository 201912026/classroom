package com.example.toddle.controller;

import com.example.toddle.model.User;
import com.example.toddle.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Objects;

@RestController
public class TestingController {

    private static final Logger log = LoggerFactory.getLogger(TestingController.class);

    @Autowired
    private UserService userService;

    @RequestMapping("hello")
    public String helloWorld(@RequestParam(value="name", defaultValue="World") String name) {
        return "Hello "+name+"!!";
    }

    @RequestMapping("home")
    public ResponseEntity<Object> home() {
        User user = new User();
        user.setUsername("zain");
        user.setPassword("password");
        user.setDateCreate(new Date());
        user.setType("tutor");
        userService.save(user);

        User user1 = new User();
        user1.setUsername("one");
        user1.setPassword("one");
//        user1.setType("STUDENT");
        user1.setDateCreate(new Date());
        userService.save(user1);

        User user2 = new User();
        user2.setUsername("two");
        user2.setPassword("two");
//        user2.setType("STUDENT");
        user2.setDateCreate(new Date());
        userService.save(user2);

        return new ResponseEntity<>("user created", HttpStatus.OK);

    }

}

package com.example.toddle.repository;

import com.example.toddle.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    @Query(value = "select u from User u where u.username=?1")
    User findUserByUsername(String name);

    User findUserByUsernameAndPassword(String username, String password);

//    User findById(Long id);

}

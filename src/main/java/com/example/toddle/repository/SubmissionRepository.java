package com.example.toddle.repository;

import com.example.toddle.model.AssignmentSubmission;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubmissionRepository extends CrudRepository<AssignmentSubmission, Long> {

    @Query( value = "select sub from AssignmentSubmission sub where sub.assignment.id=?1")
    List<AssignmentSubmission> getAllByAssignmentId(Long id);

    @Query( value = "select sub from AssignmentSubmission  sub where sub.assignment.id=?1 and sub.user.id=?2")
    AssignmentSubmission findByAssignmentId(Long id, Long studentId);

    @Query (value = "select ass from AssignmentSubmission ass where ass.assignment.id=?1")
    List<AssignmentSubmission> getAllAssignmentsByAssId(Long id);

}

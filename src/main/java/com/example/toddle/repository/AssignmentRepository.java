package com.example.toddle.repository;

import com.example.toddle.model.Assignment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssignmentRepository extends CrudRepository<Assignment, Long> {

    @Query( value = "select ass from Assignment ass where ass.name = ?1 order by ass.dateCreate desc")
    List<Assignment> findByNameAllIgnoreCase(String name);

    @Query( value = "select ass from Assignment ass where ass.tutorId.id=?1")
    List<Assignment> getAllByTutorId(Long id);

}

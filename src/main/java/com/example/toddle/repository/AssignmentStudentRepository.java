package com.example.toddle.repository;

import com.example.toddle.model.AssignmentStudent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AssignmentStudentRepository extends CrudRepository<AssignmentStudent, Long> {

    @Query( value = "select ass from AssignmentStudent ass where ass.assignment.id = ?1 and ass.user.id=?2")
    AssignmentStudent findAssignmentStudentByAssignmentIdAndUserId(Long id, Long studId);

    @Query (value = "select ass from AssignmentStudent ass where ass.user.id=?1")
    List<AssignmentStudent> findAllByStudentId(Long id);

    @Query (value = "select ass from AssignmentStudent ass where ass.assignment.id=?1")
    List<AssignmentStudent> getAllAssignmentsByAssId(Long id);
}
